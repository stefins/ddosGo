# DDos Go

A simple DDos CLI written with Go

# Installation

```console
$ go get github.com/iamstefin/ddosgo
```

# Usage

```console
$ ddosgo --help
Usage of ddosgo:
  -url string
    	The target URL
```

# Notes

This tool was created by Stef in an attempt to learn Go.
